const { response, request } = require("express");
const { default: axios } = require("axios");

const Character = require("../models/character");

const instance = axios.create({
  baseURL: `https://api.got.show/api/book/characters`,
});

//Funcion para crear y llenar base de datos desde la API de GOT en caso de que no exista ningun documento, se llenara denuevo, para esta prueba este metodo se invoca al iniciar el servidor
const fillCharacters = async () => {
  try {
    const res = await instance.get();
    if ((await Character.countDocuments()) == 0) {
      await Promise.all(
        res.data.map(async (character) => {
          return await Character.create(character);
        })
      );
    }
  } catch (error) {
    console.log(error.message);
  }
};

const getCharacters = async (req, res = response) => {
  // se desestructuran los query params para obtener el numero de items por pagina
  const { page = 1, limit = 10, search = "" } = req.query;
  try {
    //se almacenan obtienen los personajes en base a los parametros enviados en el caso del search busca por expresion regular en casa o nombre
    const characters = 
    await Character.find({
      $or:[
        {house: { $regex: search, $options: "gmi" },},
        {name: { $regex: search, $options: "gmi" }}
      ]
    })
      .select(["name", "house"])
      .limit(limit)
      .skip((page - 1) * limit);

    res.status(200).json(characters);
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

const getCharacter = async (req, res = response) => {
  const { id } = req.params;

  try {
    const character = await Character.findById(id);

    res.status(200).json(character);
  } catch (error) {
    res.status(400).json({ msg: error.message });
  }
};

module.exports = {
  getCharacters,
  getCharacter,
  fillCharacters,
};
