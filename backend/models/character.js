const { default: mongoose } = require("mongoose");
const { Schema, model } = require("mongoose");

const CharactersSchema = Schema(
  {
    _id: { type: mongoose.Types.ObjectId },
    name: { type: String },
    titles: { type: Array },
    image: { type: String, default :null },
    house: { type: String },
    gender: { type: String,default:null},
    children:{ type: Array },
    allegiance: { type: Array },
    books: { type: Array,default:[] },
    slug: { type: String, default :null },
    pagerank: { title:{type:String},rank:{type:Number} },
  },
  {
    timestamps: true,
  }
);

module.exports = model("Character", CharactersSchema);
