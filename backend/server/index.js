const express = require("express");
const cors = require("cors");
 const { dbConnection } = require("../database/config");
const { fillCharacters } = require("../controllers/characters");

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT || 4000;
    this.characters = "/api/characters";
    this.conectarDB(); 

    // Middlewares
    this.middlewares();
    // Rutas de mi aplicación
    this.routes();
  }
  async conectarDB() {
    await dbConnection();
    await fillCharacters();
  }

  middlewares() {
    // CORS
    this.app.use(cors());
    // Lectura y parseo del body
    this.app.use(express.json());
  }

  routes() {
    this.app.use(this.characters, require("../routes/characters"));
  }
  listen() {
    this.app.listen(this.port, () => {
      console.log("Servidor corriendo en puerto", this.port);
    });
  }
}

module.exports = Server;
