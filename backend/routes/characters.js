const { Router } = require("express");;
const { getCharacters, getCharacter } = require("../controllers/characters");

const router = Router();

router.get("/", getCharacters); 
router.get("/:id", getCharacter); 

module.exports = router;