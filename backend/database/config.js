const mongoose = require("mongoose");

const dbConnection = async () => {
  try {
     mongoose.connect(process.env.MONGODB_CONN, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
    console.log('base de datos online');
  } catch (error) {
    throw new Error("error conenctando a la base de datos");
  }
};

module.exports = {
  dbConnection,
};
