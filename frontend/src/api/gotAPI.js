import axios from "axios"

const instanceApi = axios.create({
   baseURL: process.env.REACT_APP_API
})

export const getAllCharacters = async(page,limit,search="stark")=>{
   const response = await instanceApi.get('/characters',{params:{page,limit,search}});
   return response.data
}

export const getCharacter = async(id)=>{
    const response = await instanceApi.get(`/characters/${id}`);
    return response.data
 }