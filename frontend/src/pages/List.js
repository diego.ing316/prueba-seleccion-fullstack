import { useState } from "react";
import { InputSearch } from "../components/InputSearch";
import { Loading } from "../components/Loading";
import { Pagination } from "../components/Pagination";
import { TableCharacters } from "../components/TableCharacters";
import { useCharacters } from "../hooks/useCharacters";

export const List = () => {
  const PAGINACION = 10;
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const { characters, isLoading } = useCharacters(currentPage, PAGINACION,search);

 

  //funcion para controlar el valor del input para buscar, en el caso de que este elemento tengan datos para buscar la pagina actual se seteara en
  const handleChange = ({ target }) => {
    setCurrentPage(1);
    setSearch(target.value);
    
  };

  return (
    <div>
      <h1>Listado Personages Game of Thrones</h1>
      <hr />
      <InputSearch search={search} handleChange={handleChange}/>
      <Pagination setCurrentPage={setCurrentPage} currentPage={currentPage}/>
      {isLoading ? <Loading /> : <TableCharacters characters={characters}/>}
    </div>
  );
};
