import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getCharacter } from "../api/gotAPI";

export const View = () => {
  const navigate = useNavigate();
  const [character, setCharacter] = useState({});
  const { characterId } = useParams();
  useEffect(() => {
    getCharacter(characterId).then((character) => {
      setCharacter(character);
    });
  }, [characterId]);
  return (
    <div className="w-100 d-flex flex-row justify-content-between">
      {character.image ? (
        <img
          src={character.image}
          style={{ maxWidth: "20%", height: "auto",marginRight:'10px'}}
          alt={character.name}
        ></img>
      ) : null}

      <div className="w-100 d-flex flex-column">
        <h2>Nombre : {character.name}</h2>
        <h2>Casa : {character.house}</h2>
        <h2>Titulos : {character?.titles?.map((titles) => `${titles}. `)}</h2>
        <h2>Libros : {character?.books?.map((book) => `${book}. `)}</h2>
        <h2>Slug : {character.slug}</h2>
        <h2>Rank : {character.pagerank?.rank}</h2>
      </div>
      <button
        className="w-25 btn btn-primary my-2"
        style={{ maxWidth: "20%", height: "50px" }}
        onClick={() => {
          navigate("/");
        }}
      >
        Volver a la lista
      </button>
    </div>
  );
};
