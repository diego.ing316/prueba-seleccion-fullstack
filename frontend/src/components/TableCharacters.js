import React from "react";
import { NavLink } from "react-router-dom";

export const TableCharacters = ({characters}) => {
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <td>Nombre</td>
            <td>Casa</td>
          </tr>
        </thead>
        <tbody>
          {characters.map(({ _id, name, house }) => {
            return (
              <tr key={_id}>
                <td>
                  <NavLink to={`view/${_id}`}>{name}</NavLink>
                </td>
                <td>{house}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
