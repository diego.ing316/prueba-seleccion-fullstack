export const Pagination = ({setCurrentPage,currentPage}) => {
  //funcion de la logica del boton siguiente
  const nextPage = () => {

    setCurrentPage((prev) => prev + 1);
  };
  //funcion de la logica del boton anterior
  const previusPage = () => {
    setCurrentPage((prev) => (prev > 1 ? prev - 1 : prev));
  };

  return (
    <div className="w-25 d-flex flex-row justify-content-between">
      <button onClick={previusPage} className="btn btn-primary">
        Anterior
      </button>
      <h3>{currentPage}</h3>
      <button onClick={nextPage} className="btn btn-primary">
        Siguiente
      </button>
    </div>
  );
};
