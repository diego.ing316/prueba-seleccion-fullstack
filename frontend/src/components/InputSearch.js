import React from "react";

export const InputSearch = ({search,handleChange}) => {
  return (
    <div className="w-50 d-flex flex-row justify-content-between">
      <input
        type="text"
        className="w-100 mb-2 form-control"
        placeholder="Buscar por nombre o casa"
        value={search}
        onChange={handleChange}
      />
    </div>
  );
};
