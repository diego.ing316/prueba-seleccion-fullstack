import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { List } from '../pages/List'
import { View } from '../pages/View'

export const AppRouter = () => {
  return (
   <Routes>
       <Route path="/view/:characterId" element={<View />} />
       <Route path="/*" element={<List />} />
   </Routes>
  )
}
