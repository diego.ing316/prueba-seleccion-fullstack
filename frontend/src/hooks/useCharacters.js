import { useEffect, useState } from "react";
import { getAllCharacters } from "../api/gotAPI";

export const useCharacters = (page,limit,search) => {
  const [isLoading, setIsLoading] = useState(true);
  const [characters, setCharacters] = useState([]);

  useEffect(() => {
    setIsLoading(true)
    getAllCharacters(page,limit,search).then(characters=>{
        setCharacters(characters);
        setIsLoading(false);
    });
  }, [page,limit,search]);

  return {
    isLoading,
    characters
  };
};
