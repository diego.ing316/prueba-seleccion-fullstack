# GAME OF THRONES APP

El sistema permite :

    1- Listar todos los personajes del universo de G.O.T.
    2- Filtrar busqueda por casa o nombre de los personajes 
    3- Seleccionar algun personaje de la lista
    4- Seleccionar el personaje y mostrar información detallada del mismo


## Front End

### Indicaciones 
- Instalar Dependencias mediante comando "yarn"
- Convertir el archivo **env.example** en **.env**, este archivo posee la variable de entorno **API** para conectarse al servicio de backend, por defecto tiene el puerto 9000.
- Por ultimo ejecutar el comando "yarn start" para iniciar el proyecto, por defecto React.Js utiliza el puerto **3000**.

## Back End 

### Indicaciones 
- Instalar dependencias con el comando "yarn"
- Convertir el archivo **env.example** en **.env**, la variable **PORT** viene seteada con **9000** y la variable **MONGODB_CONN** es la que posee el acceso y las credenciales al servidor de la base de datos.
-Ejecutar y levantar el servicio mediante "node app.js"
-Para el proyecto se creo un cluster en cloud free tier donde etan almacenados los datos.

#### Servicios 

La API posee dos end points:

- /api/characters/id : obtiene un personaje mediante su id
- /api/characters/ : obtiene una lista de personajes paginada. los query params son (**page, limit, search**) en el caso de estar vacios tienen un valores por defecto

Al iniciar la app, automaticamente consultara si esta creada la base de datos con el Schema de Characters y si no, obtendra y hara un fill a la base de datos


